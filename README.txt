This are installation and execution instructions for the project﻿

NB: The project was built using C# .NET Framework, in order to run and execute the service please have Visual Studio 2012 or new installed on your system/computer.

Running the project is relatively simply please open the kutlwano-motlhabedi.sln file, after Visual studio will open with the project.

1. After the project has successfully loaded please open the Service.svc file, Press Run on the Visual studio taskbar or press F5 to run the project
2. A web service GUI test client should pop up after compilation, then you can test the procedures on the public endpoints /Hello, /Login, etc
3. The test client has an input and output area to enter data
