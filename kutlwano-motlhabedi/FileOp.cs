﻿using System;
using System.Collections.Generic;
using System.IO;

namespace kutlwano_motlhabedi
{
    class FileOp
    {
        public const String USER_CRED_FILE = "users.txt";
        public const String USER_TOKEN_FILE = "auth.txt";

        public void Exists(String filenm)
        {
            Boolean exists = File.Exists(filenm);
            //Create file if not exists
            if (!exists)
            {
                File.Create(filenm);
            }
         
        }

        public String Read(String filenm)
        {
            String filestring = "";
            Exists(filenm);
            int counter = 0;
            string line;

            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(filenm);
            while ((line = file.ReadLine()) != null)
            {
                filestring += line+ "\r\n";
                counter++;
            }

            file.Close();
            return filestring;
        }

        public String Write(String filenm, String writeString)
        {
            //try
            //{
                Exists(filenm);
                String filestring = Read(filenm);
                // Write the string to a file.
                System.IO.StreamWriter file = new System.IO.StreamWriter(filenm);
                file.WriteLine(filestring + "\r\n" + writeString);
                file.Close();
            //}catch(IOException io)
            //{
            //    return "Error to write to file " + io.ToString();
            //}
            return "Success";
        }
    }
}
