﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace kutlwano_motlhabedi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
    [ServiceContract]
    public interface IService
    {
        /*
         * Used to test connection to web service from client
         * @return          - String
         * */
        [OperationContract]
        [WebGet]
        String Hello();

        /*
         * Used to do register process from client
         * returns a message to the user
         * @param  username - String
         *         password - String
         * @return Token    - String
         * */
        [OperationContract]
        [WebInvoke]
        String Register(String username, String password);

        /*
       * Used to authenticate login process from client
       * returns a token to the user
       * @param  username - String
       *         password - String
       * @return Token    - String
       * */
        [OperationContract] 
        [WebInvoke]
        String Login(String username, String password);

      /*
       * Used to authenticate a token from client
       * returns message to the user
       *
       * @param  username - String
       *         password - String
       * @return Token    - String
       * */
        [OperationContract]
        [WebInvoke]
        String Auth(String authtokenx);
    }
}
